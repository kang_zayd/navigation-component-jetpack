package com.andronity.navigationcomponentjetpack

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ) =
        inflater.inflate(R.layout.fragment_login, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textToRegister.setOnClickListener {
            view.findNavController().navigate(R.id.actionToRegister)
        }
        btnLogin.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("data", "Selamat Datang di Andronity Solo")
            view.findNavController().navigate(R.id.actionToHome, bundle)
        }
    }
}